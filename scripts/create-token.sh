#!/bin/bash


# enforce a display name is set 
POLICY=$1
DISPLAY_NAME=$2


die(){
  echo "ERROR: $1" >/dev/stderr
  exit 1
}

help(){
	echo "Usage: $0 policyname token-display-name"
	exit 1
}
[ -z "$POLICY" ] && help
[ -z "$DISPLAY_NAME" ] && help

vault token-create -policy="$POLICY" -display-name "$DISPLAY_NAME"

